package com.zheng;

import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * Created by zhenglian on 2016/10/9.
 */
@SpringBootApplication
public class Application {
	//手动设置初始化  语言类别
//	@Bean
//	public LocaleResolver localeResolver() {
//	    SessionLocaleResolver slr = new SessionLocaleResolver();
//	    slr.setDefaultLocale(Locale.CHINA);
//	    return slr;
//	}
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
